FROM node:20-alpine3.21

ARG WBVERSION

ARG SCVERSION=0.2.33

RUN apk add --no-cache --virtual .download ca-certificates wget && \
    apk add --no-cache nginx && \
    mkdir -p /run/nginx && touch /run/nginx/nginx.pid && \
    wget https://github.com/cracker0dks/whiteboard/archive/v$WBVERSION.tar.gz && \
    wget https://github.com/aptible/supercronic/releases/download/v$SCVERSION/supercronic-linux-amd64 && \
    chmod +x supercronic-linux-amd64 && \
    mv supercronic-linux-amd64 /usr/local/bin/supercronic && \
    tar -C /opt -xzvf v$WBVERSION.tar.gz && \
    rm v$WBVERSION.tar.gz && \
    mv /opt/whiteboard-$WBVERSION /opt/whiteboard && \
    cd /opt/whiteboard && \
    npm install && \
    npm run build && \
    apk del .download

ADD entrypoint.sh /entrypoint.sh
ADD default.conf /etc/nginx/http.d/default.conf
ADD index.html /var/www/localhost/htdocs/index.html
ADD picasoft.svg /var/www/localhost/htdocs/picasoft.svg
ADD picasoft-logo-whiteboard.png /var/www/localhost/htdocs/picasoft-logo-whiteboard.png
ADD default.css /var/www/localhost/htdocs/default.css

RUN chmod +x /entrypoint.sh && \
    chown -R nginx:nginx /var/www/localhost

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
